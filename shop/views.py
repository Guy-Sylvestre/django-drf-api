from rest_framework.views import APIView
from rest_framework.viewsets import ModelViewSet
from rest_framework.response import Response

from shop.models import Category, Product
from shop.serializers import CategorySerializer, ProductSerializer


class CategoryViewset(ModelViewSet):

   serializer_class = CategorySerializer
   
   def get_queryset(self):
       return Category.objects.all()


class ProductViewset(ModelViewSet):

    serializer_class = ProductSerializer
    
    def get_queryset(self):
        return Product.objects.all()

# class CategoryViewset(APIView):

#     def get(self, *args, **kwargs):
#         queryset = Category.objects.all()
#         serializer = CategorySerializer(queryset, many=True)
#         return Response(serializer.data)


# class ProductViewset(APIView):

#     def get(self, *args, **kwargs):
#         queryset = Product.objects.all()
#         serializer = ProductSerializer(queryset, many=True)
#         return Response(serializer.data)