from django.urls import path
from rest_framework import routers
# from rest_framework.routers import DefaultRouter

from .views import CategoryViewset


# urlpatterns = [
#     path('category/', CategoryView.as_view())
# ]

# router =  DefaultRouter()
router = routers.SimpleRouter()
router.register('category', CategoryViewset, basename='category' )